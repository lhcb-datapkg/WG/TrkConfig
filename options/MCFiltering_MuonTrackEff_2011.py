"""
Options for building Stripping21r1p2. 
"""

#use CommonParticlesArchive
stripping='stripping21r1p2'

my_wg=['B2OC', 'B2CC', 'BandQ', 'Semileptonic', 'RD', 'Charm', 'QEE', 'BnoC', 'Calib']

from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

from Configurables import GaudiSequencer, RawEventJuggler
jseq=GaudiSequencer("RawEventSplitSeq")
juggler=RawEventJuggler("rdstjuggler")
juggler.Sequencer=jseq
juggler.Input="Reco14"
juggler.Output=4.3

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive, WGs = my_wg)

AllStream = StrippingStream("MuonTrackEff.Strip")

# These lines are needed to remove duplicates (everything goes to the same stream)
muon_track_eff = lambda line_name : all(keyword in line_name for keyword in ['StrippingTrackEff', 'Muon']) and not 'DownMuonValidation' in line_name
# Put stuff in a dict to remove duplicates
lines_dict = {line.name() : line for stream in streams for line in stream.lines if muon_track_eff(line.name())}
# Get a list of lines
lines_to_add = [lines_dict[name] for name in lines_dict]
# Change the prescale
for line in lines_to_add:
    line._prescale = 1.0

AllStream.appendLines(lines_to_add)

from PhysConf.Filters import LoKi_Filters
flts = LoKi_Filters(VOID_Code = "( TrSource(TrSOURCE('/Event/Rec/Track/Best', TrLONG))"\
                                " >> ( sum( TrPT,TrP < 1 * TeV ) > 1 * TeV ) )" ,
                    VOID_Preambulo = ["from LoKiTracks.decorators import *" ,
                                      "from LoKiCore.functions    import * ",
                                      "from GaudiKernel.SystemOfUnits import *"])



sc = StrippingConf( Streams = [AllStream],
                    MaxCandidates = 2000,
                    #MaxCombinations = 10000000,
                    TESPrefix = 'Strip' )

AllStream.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=True,fileExtension='.ldst')
    }

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs



dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x39162112)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().prependToMainSequence( [jseq] )
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
